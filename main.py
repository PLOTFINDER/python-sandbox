import pygame, sys, time
from pygame.locals import *

from board import Board


pygame.init()

window = pygame.display.set_mode((640, 480))
pygame.display.set_caption("Sand Box")
mainClock = pygame.time.Clock()

board = Board(64,48,10)
# print(board.blockList)
game = True
mouse_pos = None

framerate = 60
time_per_frame = 1/60
last_time = time.time()

frameCount = 0


def renderBoard(board):
    window.fill((0,0,0))
    blocks = board.getBlocks()
    bSize = board.blockSize
    for y in range(len(blocks)):
        for x in range(len(blocks[0])):

            pX = x*bSize
            pY = y*bSize

            if blocks[y][x] == 1:
                pygame.draw.rect(window, "yellow", (pX, pY, bSize, bSize))
            elif int(blocks[y][x]) == 2:
                pygame.draw.rect(window, "blue", (pX, pY, bSize, bSize))


while game:
    delta_time = time.time() - last_time
    while delta_time < time_per_frame:
        delta_time = time.time() - last_time

        for event in pygame.event.get():

            if event.type == QUIT:
                game = not game

            if event.type == KEYDOWN:
                print(event.key)

            if event.type == MOUSEMOTION:
                mouse_pos = event.pos
                # print(mouse_pos)

            if event.type == MOUSEBUTTONDOWN:
                # print(mouse_pos)
                    board.selected(mouse_pos, event.button)

    if frameCount%6 == 0:
        # print(frameCount//60)
        board.update()

    renderBoard(board)
    pygame.display.update()
    last_time = time.time()
    frameCount += 1

    # mainClock.tick(framerate)






if __name__ == '__main__':
    pass