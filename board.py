from vector import Vector
class Board:

    blockList = None
    size = None
    blockSize = None

    PATTERN = [Vector(0,1),Vector(-1,1),Vector(1,1),Vector(-1,0),Vector(1,0)]


    def __init__(self, x, y, bSize):
        self.size = Vector(x,y)
        self.blockSize = bSize
        self.blockList = [[0 for i in range(x)] for j in range(y)]

    def getBlocks(self)-> list:
        """Getter for blockList from Board class"""
        return self.blockList

    def update(self):
        self.collision()

    def selected(self, mPos, button):
        x = mPos[0] // self.blockSize
        y = mPos[1] // self.blockSize
        # print((x,y))
        if button == 3 : button = 2
        self.blockList[y][x] = button




    def isHere(self, x ,y):
        return self.blockList[y][x] !=0

    def whoIsHere(self, x, y):
        return int(self.blockList[y][x])

    def isOutside(self, x, y):
        return x >= 0 and x < self.size.x and y >= 0 and y < self.size.y

    def move(self, x, y, pattern):
        if self.isOutside(x + pattern.x, y + pattern.y):
            if not self.isHere(x + pattern.x, y + pattern.y):
                return 1
            else:
                if self.whoIsHere(x,y) != 2 and int(self.whoIsHere(x + pattern.x, y + pattern.y)) == 2:
                    return 2

        return 0

    def collision(self):

        self.blockList = [[int(x) for x in y] for y in self.blockList]

        blocks = self.blockList
        for y in range(len(blocks)-1, -1, -1):
            for x in range(len(blocks[0])-1, -1, -1):
                if blocks[y][x] == 1:
                    self.sandCollision(x,y)
                elif blocks[y][x] == 2:
                    self.waterCollision(x,y)

    def sandCollision(self, x, y):
        blocks = self.blockList
        steps = 1
        for i,pattern in enumerate(self.PATTERN):
            if i < 3 and steps==1:
                res = self.move(x,y,pattern)
                if res == 1:
                    blocks[y][x] = 0
                    blocks[y+pattern.y][x+pattern.x] = 1
                    steps-=1
                elif res == 2:
                    blocks[y][x] = 2
                    blocks[y+pattern.y][x+pattern.x] = 1
                    steps-=1


    def waterCollision(self, x, y):
        blocks = self.blockList
        for i,pattern in enumerate(self.PATTERN):
                res = self.move(x,y,pattern)
                if res == 1:
                    blocks[y][x] = 0
                    blocks[y+pattern.y][x+pattern.x] = 2 + 0.1
                    break

