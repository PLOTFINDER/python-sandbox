
class Vector:
    def __init__(self, x, y, z=0):
        self.x = x
        self.y = y
        self.z = z

    def getX(self)-> float:
        """Getter for x from Vector class"""
        return self.x

    def getY(self)-> float:
        """Getter for y from Vector class"""
        return self.y

    def getZ(self)-> float:
        """Getter for z from Vector class"""
        return self.z

    def setX(self, x):
        """Setter for x from Vector class"""
        self.x = x

    def setY(self, y):
        """Setter for y from Vector class"""
        self.y = y

    def setZ(self, z):
        """Setter for z from Vector class"""
        self.z = z

    def getPos(self)-> tuple:
        """Getter for x,y,z from Vector class"""
        return (self.x,self.y,self.z)
